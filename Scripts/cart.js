function loadCart(){
    var cartElement = document.getElementById("usercart");

    if (sessionStorage.getItem("Cart") != null)
    {
        cartElement.innerHTML = `
            <img src="Images/cart.jpg" alt="Cart" class="cart" onclick="sendOrder()">
            <h2> (${sessionStorage.getItem("Cart")})</h2>`;
    }

}


function addToCart(){
    var counter = 0;
    if (sessionStorage.getItem("Cart") != null)
    {
        counter = sessionStorage.getItem("Cart");
    }

    counter++;

    var cartElement = document.getElementById("usercart");
    cartElement.innerHTML =  `
        <img src="Images/cart.jpg" alt="Cart" class="cart" onclick="sendOrder()">
        <h2> (${counter})</h2>`;
    sessionStorage.setItem("Cart", counter);
}

function sendOrder(){
    var counter = 0;
    var cartElement = document.getElementById("usercart");
    if (sessionStorage.getItem("Cart") != null)
    {
        counter = sessionStorage.getItem("Cart");
    }

    if (counter > 0)
    {
        alert("Thank you for your order!");
        sessionStorage.setItem("Cart", 0);
        cartElement.innerHTML =  `
            <img src="Images/cart.jpg" alt="Cart" class="cart" onclick="sendOrder()">
            <h2> (${sessionStorage.getItem("Cart")})</h2>`;
    }
    else
    {
        alert("Please add something to the cart");
    }
}