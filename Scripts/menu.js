function getMenu(filename) {
    switch (filename) {
        case "pizzas":
            addPizzaItems();
            break;

        case "salads":
            addSaladItems();
            break;

        case "softdrinks":
            addSoftdrinkItems();
            break;

        default:
            alert("Menu could not be found!");
            break;
    }
}

function addPizzaItems(){
    let pizzaItems = [
        {
            "name": "Piccante",
            "prize": "16$",
            "id": 1,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Spicy Salami",
                "Chilies",
                "Oregano"
            ],
            "imageUrl": "https://live.staticflickr.com/8449/8056443961_7e398665bf_3k.jpg"
        },
        {
            "name": "Giardino",
            "prize": "14$",
            "id": 2,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Artichokes",
                "Fresh Mushrooms"
            ],
            "imageUrl": "https://live.staticflickr.com/2746/4277585664_f08c8e3aaa_k.jpg"
        },
        {
            "name": "Prosciuotto e funghi",
            "prize": "15$",
            "id": 3,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Ham",
                "Fresh Mushrooms",
                "Oregano"
            ],
            "imageUrl": "https://live.staticflickr.com/5596/18261069223_b2d67f4f4a_b.jpg"
        },
        {
            "name": "Quattro formaggi",
            "prize": "13$",
            "id": 4,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Parmesan",
                "Gorgonzola"
            ],
            "imageUrl": "https://live.staticflickr.com/65535/53171373976_6937e571bf_3k.jpg"
        },
        {
            "name": "Quattro stagioni",
            "prize": "17$",
            "id": 5,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Ham",
                "Artichokes",
                "Fresh Mushrooms"
            ],
            "imageUrl": "https://live.staticflickr.com/193/454537389_b9a0f6bb8d_k.jpg"
        },
        {
            "name": "Stromboli",
            "prize": "12$",
            "id": 6,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Fresh Chilies",
                "Olives",
                "Oregano"
            ],
            "imageUrl": "https://live.staticflickr.com/7335/9670112805_9bce553d1f_k.jpg"
        },
        {
            "name": "Verde",
            "prize": "13$",
            "id": 7,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Broccoli",
                "Spinach",
                "Oregano"
            ],
            "imageUrl": "https://live.staticflickr.com/7148/6772606885_c141609b4f_4k.jpg"
        },
        {
            "name": "Rustica",
            "prize": "15$",
            "id": 8,
            "ingredients": [
                "Tomato",
                "Mozzarella",
                "Ham",
                "Bacon",
                "Onions",
                "Garlic",
                "Oregano"
            ],
            "imageUrl": "https://live.staticflickr.com/636/22287619414_ece963d83e_4k.jpg"
        }
    ];
    let menu = document.getElementById("menu");
    var contentToAdd = "";
    pizzaItems.forEach(element => {
        var ingredients = "";
        element.ingredients.forEach(ingredient => ingredients = ingredients + ingredient + ", ");

        contentToAdd += 
            `<div>
                <img src="${element.imageUrl}" alt="${element.name}">
                <div class="price">
                    <h2>${element.name}</h2>
                    <div>
                        <h2>${element.prize}</h2>
                        <button onclick="addToCart()"><img src="Images/cart.jpg" class="cart"></button>
                    </div>
                </div>
                <p>${ingredients.substring(0, ingredients.length-2)}</p>
            </div>`;    
        });
    menu.innerHTML = contentToAdd;
}

function addSaladItems(){
    let saladItems = [
        {
            "name": "Green salad with tomatoe",
            "prize": "4$",
            "id": 1,
            "ingredients": [
                "Iceberg lettuce",
                "Tomatoes"
            ],
            "dressings": [
                "Italian dressing",
                "French dressing",
                "No dressing"
            ],
            "imageUrl": "https://live.staticflickr.com/5625/22804417144_93cfb19f21_k.jpg"
        },
        {
            "name": "Tomato salad with mozzarella",
            "prize": "5$",
            "id": 2,
            "ingredients": [
                "Tomato",
                "Mozzarella"
            ],
            "dressings": [
                "Italian dressing",
                "French dressing",
                "No dressing"
            ],
            "imageUrl": "https://live.staticflickr.com/4142/4859492625_2c6e70b470_k.jpg"
        },
        {
            "name": "Field salad with egg",
            "prize": "4$",
            "id": 3,
            "ingredients": [
                "Field salad",
                "Egg"
            ],
            "dressings": [
                "Italian dressing",
                "French dressing",
                "No dressing"
            ],
            "imageUrl": "https://live.staticflickr.com/8664/16110275664_0649a285d6_z.jpg"
        },
        {
            "name": "Rocket with parmesan",
            "prize": "5$",
            "id": 4,
            "ingredients": [
                "Rocket",
                "Parmesan"
            ],
            "dressings": [
                "Italian dressing",
                "French dressing",
                "No dressing"
            ],
            "imageUrl": "https://live.staticflickr.com/7017/6818343859_33f2261807_5k.jpg"
        }
    ];
    let menu = document.getElementById("menu");
    var contentToAdd = "";
    saladItems.forEach(element => {
        var ingredients = "";
        element.ingredients.forEach(ingredient => ingredients = ingredients + ingredient + ", ");
        var dressings = "";
        element.dressings.forEach(dressing => dressings += `<option>${dressing}</option>`)
        contentToAdd += 
        `<div>
            <img src="${element.imageUrl}" alt="${element.name}">
            <h2>${element.name}</h2>
            <p>${ingredients.substring(0, ingredients.length-2)}</p>
            <div class="price">
                <select>
                    ${dressings}
                </select>
                <div>
                    <h2>${element.prize}</h2>
                    <button onclick="addToCart()"><img src="Images/cart.jpg" class="cart"></button>
                </div>
            </div>
        </div>`
    });
    menu.innerHTML = contentToAdd;
}

function addSoftdrinkItems(){
    let softdrinkItems = [
        {
            "name": "Coke",
            "prize": "2$",
            "id": 1,
            "imageUrl": "https://live.staticflickr.com/8632/16087121576_65f2a018b1_3k.jpg",
            "volumes": [
                "50cl",
                "100cl",
                "150cl"
            ]
        },
        {
            "name": "Fanta",
            "prize": "2$",
            "id": 2,
            "imageUrl": "https://live.staticflickr.com/8396/8644522129_36be341b5a_4k.jpg",
            "volumes": [
                "50cl",
                "100cl",
                "150cl"
            ]
        },
        {
            "name": "Pepsi",
            "prize": "2$",
            "id": 3,
            "imageUrl": "https://live.staticflickr.com/3638/3701025253_b2e5e1d08a_c.jpg",
            "volumes": [
                "50cl",
                "100cl",
                "150cl"
            ]
        },
        {
            "name": "Red bull",
            "prize": "3$",
            "id": 4,
            "imageUrl": "https://live.staticflickr.com/4017/4487927910_222796703e_k.jpg",
            "volumes": [
                "50cl",
                "100cl",
                "150cl"
            ]
        }
    ];
    let menu = document.getElementById("menu");
    var contentToAdd = "";
    softdrinkItems.forEach(element => {
        var volumes = "";
        element.volumes.forEach(volume => volumes += `<option>${volume}</option>`);
        console.log(volumes);
        contentToAdd += 
        `<div>
            <img src="${element.imageUrl}" alt="${element.name}">
            <h2>${element.name}</h2>
            <div class="price">
                <select>
                    ${volumes}
                </select>
                <div>
                    <h2>${element.prize}</h2>
                    <button onclick="addToCart()"><img src="Images/cart.jpg" class="cart"></button>
                </div>
            </div>
        </div>`
    });
    menu.innerHTML = contentToAdd;
}